# UkuliWork
## Server branches
* Master (Distribution version, should be the same as production).
* Production (The one running in production)
* Staging (The one we run various tests against)
* Development (Here we merge the issue branches)
## Client repos
* [Desktop Client](https://github.com/TomiToivio/ukuli-work-desktop-client)
* [Mobile Client](https://github.com/TomiToivio/ukuli-work-mobile-client)
