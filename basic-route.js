const express = require('express');
const app = express();

/* Front page */
app.get('/', (request, response, nextHandler) => {
    response.status(200).send('Ukuli Data Net Home');
});

/* Register page */
app.get('/register', (request, response, nextHandler) => {
    response.status(200).send('Ukuli Data Net Register');
});

/* Freelancer Map Page */
app.get('/map', (request, response, nextHandler) => {
    response.status(200).send('Ukuli Data Net Map');
});

/* Freelancer List Page */
app.get('/list/:page', (request, response, nextHandler) => {
    response.status(200).send('Ukuli Data Net List');
});

/* Public profile page */
app.get('/profile/:userId', (request, response, nextHandler) => {
    response.status(200).send('Ukuli Data Net User Public Profile');
});

/* Own profile page */
app.get('/profile/me', (request, response, nextHandler) => {
    response.status(200).send('Ukuli Data Net User Own Profile');
});1




app.listen(1337,
    () => console.log('Ukuli Data Net listening on port 1337'),
);
